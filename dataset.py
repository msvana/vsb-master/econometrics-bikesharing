"""
This script creates a dataset for my school econometry project by
counting all bike uses in one day and adding information about
weather conditions and if a given day was a workday or a holiday.

This dataset will be used to explain the bike demand by other variables.
"""
import os
import sys

import dateutil
import pandas

BASE_PATH = os.path.dirname(__file__)
BIKES_DATA_PATH = os.path.join(BASE_PATH, 'data', 'bikesharing')
WEATHER_DATA_PATH = os.path.join(BASE_PATH, 'data', 'weather.csv')
HOLIDAY_LIST_PATH = os.path.join(BASE_PATH, 'data', 'holidays.csv')


def main():
    """
    Create and store complete dataset containing daily bike counts and corresponding weather data
    Result is stored as CSV to a file determined by the only command line argument of the script
    """
    destination = sys.argv[1]

    bikesharing_data = load_bikesharing_data(BIKES_DATA_PATH)
    bike_counts_by_date = count_bikes_by_dates(bikesharing_data)

    weather_history = get_weather_history(WEATHER_DATA_PATH)
    are_workdays = mark_workdays(bike_counts_by_date['date'], HOLIDAY_LIST_PATH)

    complete_dataset = pandas.merge(bike_counts_by_date, weather_history, on='date', how='inner')
    complete_dataset = pandas.merge(complete_dataset, are_workdays, on='date', how='inner')
    complete_dataset = complete_dataset.round(3)
    complete_dataset.to_csv(destination, index=False)


def load_bikesharing_data(path: str) -> pandas.DataFrame:
    """
    Load raw bikesharing data downloaded from https://www.capitalbikeshare.com/system-data
    and stored as CSV files on the disk. The website provides separate CSV file
    for different time periods (months or quarters). These are contcatenated into one big Pandas DataFrame.
    """
    csv_files = [os.path.join(path, f) for f in os.listdir(path) if f.endswith('.csv')]
    dfs = [pandas.read_csv(f, parse_dates=['Start date']) for f in csv_files]
    bikesharing_df = pandas.concat(dfs)
    return bikesharing_df


def count_bikes_by_dates(bikesharing_data: pandas.DataFrame) -> pandas.DataFrame:
    """
    Take raw bikesharing data and count how many bikes were
    used during each day (determined by 'Start date' column)
    Returned DataFrame has two columns: 'date' and 'bikes_cnt'
    """
    bike_counts_by_date = bikesharing_data.groupby(bikesharing_data['Start date'].dt.date).size()
    bike_counts_by_date = bike_counts_by_date.reset_index(name='bikes_cnt')
    bike_counts_by_date = bike_counts_by_date.rename(index=str, columns={'Start date': 'date'})
    return bike_counts_by_date


def get_weather_history(path) -> pandas.DataFrame:
    """
    Load weather data for the same time period as bikesharing data.
    The dataset was dowloaded from https://www.ncdc.noaa.gov/cdo-web/datasets/.

    The dataset contains measurement from multiple stations.
    Rows not containing information about average temperature and wind speed are removed.
    Remaining data from multiple stations are averaged.
    """
    weather_df = pandas.read_csv(path, parse_dates=['DATE'])
    interesting_cols = ['DATE', 'AWND', 'TAVG', 'PRCP', 'SNOW', 'SNWD']
    weather_df = weather_df[interesting_cols]

    for col in interesting_cols:
        weather_df = weather_df[weather_df[col].notnull()]

    weather_means = weather_df.fillna(0).groupby(weather_df['DATE'].dt.date).mean().reset_index()
    weather_means.columns = [col.lower() for col in weather_means.columns]

    return weather_means


def mark_workdays(dates: pandas.Series, holiday_list_path: str) -> pandas.DataFrame:
    """
    Create a DataFrame containing information about whether a days in given series are workdays or not
    (weekends or holiday). This is marked using values 1 and 0.
    """
    with open(holiday_list_path, 'r') as holiday_file:
        holidays = holiday_file.readlines()
        holidays = [dateutil.parser.parse(h).date() for h in holidays]
        is_workday = [0 if date in holidays or date.weekday() > 4 else 1 for date in dates]
        workdays_df = pandas.DataFrame(list(zip(dates, is_workday)), columns=['date', 'is_workday'])
        return workdays_df


if __name__ == '__main__':
    main()
