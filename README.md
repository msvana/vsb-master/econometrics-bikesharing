# Washington D.C. Bikesharing data analysis

This repository serves a basis for my Econcometrics course project -
predicting number of bikes used based mostly on weather condiditions.

Raw data can be found here as well as a script creating dataset for
regression.

## Data sources

- Bikesharing: http://capitalbikeshare.com/system-data
- Weather: https://www.ncdc.noaa.gov/cdo-web/datasets
- Holiday: http://dchr.dc.gov/page/holiday-schedule

## Preprocessing

The `dataset.py` script combines data from multiple source: bikesharing
records, weather information and list of holidays for 2016, 2017 and
2018 and creates a single CSV file containing data interestring for
regression. Output path is determined by first command line argument:

```bash
python dataset.py output.csv
```

### Output format

The preprocessing script creates a CSV file containing following
columns:

- `date`: formatted (YYYY-MM-DD)
- `bikes_cnt`: number of bikes borrowed
- `awnd`: average wind speed in m/s
- `tavg`: average temperature in degrees Celsius
- `prcp`: precipitation in mm
- `snow`: snowfall in mm
- `snwd`: snow depth in mm
- `is_workday`: is given day a workday (1) or not (0). Takes weekends and public holiday into consideration.
